<?php
declare(strict_types=1);

namespace Solution;

use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Solution\Exception\BadRequestException;
use Solution\Exception\WatchNotFoundException;
use Solution\SourceWrapper\ISourceWrapper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class WatchController
{
    public const RESULT_KEY_IDENTIFICATION = 'identification';
    public const RESULT_KEY_TITLE = 'title';
    public const RESULT_KEY_PRICE = 'price';
    public const RESULT_KEY_DESCRIPTION = 'description';

    /** @var Cache */
    private $cache;

    /** @var ISourceWrapper */
    private $wrapper;

    /**
     * @param IStorage       $storage
     * @param ISourceWrapper $wrapper
     */
    public function __construct(IStorage $storage, ISourceWrapper $wrapper)
    {
        $this->cache = new Cache($storage);
        $this->wrapper = $wrapper;
    }

    /**
     * @param mixed $id
     * @return Response
     */
    public function getByIdAction($id)
    {
        try {
            $intId = $this->validateId($id);
            $watchData = $this->cache->load(
                $intId,
                function () use ($intId) {
                    return $this->wrapper->getWatchData($intId);
                }
            );
            return $this->createOkResponse($watchData);
        } catch (BadRequestException $e) {
            return $this->createBadRequestResponse();
        } catch (WatchNotFoundException $e) {
            return $this->createNotFoundErrorResponse();
        } catch (\Exception $e) {
            return $this->createServerErrorResponse();
        }
    }

    /**
     * @param $paramValue
     * @return int
     * @throws \Solution\Exception\BadRequestException
     */
    private function validateId($paramValue): int
    {
        $paramValue = (string)$paramValue;
        if (!preg_match('/^\\d+$/', $paramValue)) {
            throw new BadRequestException('Parameter value is not matched as non negative integer by regexp.');
        }

        $paramValue = (int)$paramValue;
        if ($paramValue < 1) {
            throw new BadRequestException('Parameter value is not integer greater than zero.');
        }

        return $paramValue;
    }

    /**
     * @param array $watchData
     * @return Response
     */
    private function createOkResponse(array $watchData): Response
    {
        return new JsonResponse($watchData, Response::HTTP_OK);
    }

    /**
     * @return Response
     */
    private function createBadRequestResponse(): Response
    {
        return new JsonResponse(['message' => 'Invalid value of id parameter.'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return Response
     */
    private function createServerErrorResponse(): Response
    {
        return new JsonResponse(['message' => 'Unexpected error occurred.'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @return Response
     */
    private function createNotFoundErrorResponse(): Response
    {
        return new JsonResponse(['message' => 'Watch not found.'], Response::HTTP_NOT_FOUND);
    }
}
