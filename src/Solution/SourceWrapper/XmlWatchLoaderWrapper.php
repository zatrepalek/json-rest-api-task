<?php
declare(strict_types=1);

namespace Solution\SourceWrapper;

use Assignment\XmlLoaderException;
use Assignment\XmlWatchLoader;
use Solution\Exception\InternalErrorException;
use Solution\Exception\WatchNotFoundException;
use Solution\WatchController;

class XmlWatchLoaderWrapper implements ISourceWrapper
{
    /** @var XmlWatchLoader */
    private $loader;

    /**
     * @param XmlWatchLoader $loader
     */
    public function __construct(XmlWatchLoader $loader)
    {
        $this->loader = $loader;
    }

    /**
     * @param int $id
     * @return array
     * @throws WatchNotFoundException
     * @throws InternalErrorException
     */
    public function getWatchData(int $id): array
    {
        try {
            $data = $this->loader->loadByIdFromXml((string)$id);
            if ($data === null) {
                throw new WatchNotFoundException('Watch not found in XML.');
            }
        } catch (XmlLoaderException $e) {
            throw new InternalErrorException('Internal error while parsing XML.');
        }

        return [
            WatchController::RESULT_KEY_IDENTIFICATION => (int)$data['id'],
            WatchController::RESULT_KEY_TITLE => (string)$data['title'],
            WatchController::RESULT_KEY_PRICE => (int)$data['price'],
            WatchController::RESULT_KEY_DESCRIPTION => (string)$data['desc'],
        ];
    }
}
