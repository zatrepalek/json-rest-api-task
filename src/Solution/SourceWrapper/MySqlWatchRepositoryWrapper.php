<?php
declare(strict_types=1);

namespace Solution\SourceWrapper;

use Assignment\MySqlRepositoryException;
use Assignment\MySqlWatchNotFoundException;
use Assignment\MySqlWatchRepository;
use Solution\Exception\InternalErrorException;
use Solution\Exception\WatchNotFoundException;
use Solution\WatchController;

class MySqlWatchRepositoryWrapper implements ISourceWrapper
{
    /** @var MySqlWatchRepository */
    private $repository;

    /**
     * @param MySqlWatchRepository $repository
     */
    public function __construct(MySqlWatchRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $id
     * @return array
     * @throws InternalErrorException
     * @throws WatchNotFoundException
     */
    public function getWatchData(int $id): array
    {
        try {
            $dto = $this->repository->getWatchById($id);
        } catch (MySqlWatchNotFoundException $e) {
            throw new WatchNotFoundException('Watch not found in Mysql.');
        } catch (MySqlRepositoryException $e) {
            throw new InternalErrorException('Internal error while querying Mysql.');
        }

        return [
            WatchController::RESULT_KEY_IDENTIFICATION => $dto->id,
            WatchController::RESULT_KEY_TITLE => $dto->title,
            WatchController::RESULT_KEY_PRICE => $dto->price,
            WatchController::RESULT_KEY_DESCRIPTION => $dto->description,
        ];
    }
}
