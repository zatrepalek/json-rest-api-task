<?php
declare(strict_types=1);

namespace Solution\SourceWrapper;

use Solution\Exception\InternalErrorException;
use Solution\Exception\WatchNotFoundException;

interface ISourceWrapper
{
    /**
     * @param int $id
     * @return array
     * @throws WatchNotFoundException
     * @throws InternalErrorException
     */
    public function getWatchData(int $id): array;
}
