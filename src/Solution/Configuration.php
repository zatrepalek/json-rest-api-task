<?php
declare(strict_types=1);

namespace Solution;

use Nette\Neon\Neon;

class Configuration
{
    /** @var string */
    private $filename;

    /** @var array|null */
    private $configuration = null;

    /**
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return array
     * @throws \RuntimeException
     */
    public function getConfiguration(): array
    {
        if ($this->configuration === null) {
            if (!file_exists($this->filename)) {
                throw new \RuntimeException(sprintf('Configuration file "%s" does not exists.', $this->filename));
            }
            $contents = file_get_contents($this->filename);
            if (!is_string($contents)) {
                throw new \RuntimeException(sprintf('Error while reading configuration file "%s".', $this->filename));
            }
            $this->configuration = Neon::decode($contents);
        }
        return $this->configuration;
    }
}
