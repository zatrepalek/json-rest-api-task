<?php
declare(strict_types=1);

namespace Solution\Exception;

class InternalErrorException extends \RuntimeException
{
}
