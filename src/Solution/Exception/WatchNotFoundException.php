<?php
declare(strict_types=1);

namespace Solution\Exception;

class WatchNotFoundException extends \RuntimeException
{
}
