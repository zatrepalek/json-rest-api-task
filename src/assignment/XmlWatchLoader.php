<?php
declare(strict_types=1);

namespace Assignment;

interface XmlWatchLoader
{
    /**
     * @param string $watchIdentification
     * @return array|null
     *
     * @throws XmlLoaderException
     *
     *
     * Downloads and parses a XML file containing data about watches. When no exception is thrown and the watch is not
     * found, the null value is returned. Otherwise the method returns an associative array having the following
     * structure:
     * [
     *     'id'    => 'INTEGER',
     *     'title' => 'STRING',
     *     'price' => 'INTEGER',
     *     'desc'  => 'STRING'
     * ]
     */
    public function loadByIdFromXml(string $watchIdentification): ?array;
}
