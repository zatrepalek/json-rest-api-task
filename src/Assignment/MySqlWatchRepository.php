<?php
declare(strict_types=1);

namespace Assignment;

interface MySqlWatchRepository
{
    /**
     * @param int $id
     * @return MySqlWatchDTO
     * @throws MySqlWatchNotFoundException
     * @throws MySqlRepositoryException
     **/

    public function getWatchById(int $id): MySqlWatchDTO;
}
