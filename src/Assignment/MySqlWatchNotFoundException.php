<?php
declare(strict_types=1);

namespace Assignment;

class MySqlWatchNotFoundException extends MySqlRepositoryException
{
}
