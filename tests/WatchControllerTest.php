<?php
declare(strict_types=1);

namespace Tests;

use Nette\Caching\IStorage;
use Nette\Caching\Storages\FileStorage;
use PHPUnit\Framework\TestCase;
use Solution\Exception\BadRequestException;
use Solution\Exception\WatchNotFoundException;
use Solution\SourceWrapper\ISourceWrapper;
use Solution\WatchController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Tests\Assets\TestingAccessTrait;

class WatchControllerTest extends TestCase
{
    use TestingAccessTrait;

    /**
     * @return WatchController
     */
    private function createController(): WatchController
    {
        $iStorageMock = $this->createMock(IStorage::class);
        $iSourceWrapperMock = $this->createMock(ISourceWrapper::class);

        /** @var IStorage $iStorage */
        $iStorage = $iStorageMock;
        /** @var ISourceWrapper $iSourceWrapper */
        $iSourceWrapper = $iSourceWrapperMock;
        return new WatchController($iStorage, $iSourceWrapper);
    }

    public function testCreateFailResponses()
    {
        $methodsCodes = [
            'createBadRequestResponse' => Response::HTTP_BAD_REQUEST,
            'createServerErrorResponse' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'createNotFoundErrorResponse' => Response::HTTP_NOT_FOUND,
        ];

        $controller = $this->createController();

        foreach ($methodsCodes as $method => $code) {
            $response = $this->invokeMethod($controller, $method);
            $this->assertSame(JsonResponse::class, get_class($response));
            $this->assertSame($code, $response->getStatusCode());
        }
    }

    public function testCreateOkResponse()
    {
        $controller = $this->createController();

        $response = $this->invokeMethod($controller, 'createOkResponse', [[]]);
        $this->assertSame(JsonResponse::class, get_class($response));
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testValidateId()
    {
        $controller = $this->createController();

        $this->expectException(BadRequestException::class);
        $this->invokeMethod($controller, 'validateId', ['foo']);
    }

    public function testValidateId2()
    {
        $controller = $this->createController();

        $this->expectException(BadRequestException::class);
        $this->invokeMethod($controller, 'validateId', ['0']);
    }

    public function testValidateId3()
    {
        $value = '123';
        $expected = 123;
        $controller = $this->createController();

        $this->assertSame(
            $expected,
            $this->invokeMethod($controller, 'validateId', [$value])
        );
    }

    public function testGetByIdAction()
    {
        $data = ['foo' => 'bar'];
        $cacheDir = __DIR__ . '/temp';

        // Delete all caches
        $files = glob($cacheDir . '/*');
        foreach($files as $file) {
            if(is_file($file) && $file !== '.gitignore') {
                unlink($file);
            }
        }

        $iSourceWrapperMock = $this->createMock(ISourceWrapper::class);
        $iSourceWrapperMock->expects($this->once())
            ->method('getWatchData')
            ->willReturn($data);

        /** @var IStorage $iStorage */
        $storage = new FileStorage(__DIR__ . '/temp');

        /** @var ISourceWrapper $iSourceWrapper */
        $iSourceWrapper = $iSourceWrapperMock;
        $controller = new WatchController($storage, $iSourceWrapper);

        $this->assertSame(json_encode($data), $controller->getByIdAction(10)->getContent());
        $this->assertSame(json_encode($data), $controller->getByIdAction(10)->getContent());
        $this->assertSame(json_encode($data), $controller->getByIdAction(10)->getContent());
    }

    public function testGetByIdAction2()
    {
        $iStorageMock = $this->createMock(IStorage::class);
        $iSourceWrapperMock = $this->createMock(ISourceWrapper::class);
        $iSourceWrapperMock->expects($this->once())
            ->method('getWatchData')
            ->willThrowException(new BadRequestException());

        /** @var IStorage $iStorage */
        $iStorage = $iStorageMock;
        /** @var ISourceWrapper $iSourceWrapper */
        $iSourceWrapper = $iSourceWrapperMock;
        $controller = new WatchController($iStorage, $iSourceWrapper);

        $this->assertSame(Response::HTTP_BAD_REQUEST, $controller->getByIdAction(10)->getStatusCode());
    }

    public function testGetByIdAction3()
    {
        $iStorageMock = $this->createMock(IStorage::class);
        $iSourceWrapperMock = $this->createMock(ISourceWrapper::class);
        $iSourceWrapperMock->expects($this->once())
            ->method('getWatchData')
            ->willThrowException(new WatchNotFoundException());

        /** @var IStorage $iStorage */
        $iStorage = $iStorageMock;
        /** @var ISourceWrapper $iSourceWrapper */
        $iSourceWrapper = $iSourceWrapperMock;
        $controller = new WatchController($iStorage, $iSourceWrapper);

        $this->assertSame(Response::HTTP_NOT_FOUND, $controller->getByIdAction(10)->getStatusCode());
    }

    public function testGetByIdAction4()
    {
        $iStorageMock = $this->createMock(IStorage::class);
        $iSourceWrapperMock = $this->createMock(ISourceWrapper::class);
        $iSourceWrapperMock->expects($this->once())
            ->method('getWatchData')
            ->willThrowException(new \Exception());

        /** @var IStorage $iStorage */
        $iStorage = $iStorageMock;
        /** @var ISourceWrapper $iSourceWrapper */
        $iSourceWrapper = $iSourceWrapperMock;
        $controller = new WatchController($iStorage, $iSourceWrapper);

        $this->assertSame(Response::HTTP_INTERNAL_SERVER_ERROR, $controller->getByIdAction(10)->getStatusCode());
    }
}
