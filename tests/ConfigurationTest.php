<?php
declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;
use Solution\Configuration;

class ConfigurationTest extends TestCase
{
    /** @var mixed */
    public static $fileGetContentsOverridden;

    public function testGetConfiguration()
    {
        $config = new Configuration(__DIR__ . '/Assets/sampleConfig.neon');

        $expected = [
            'cacheStorageService' => 'Solution/SourceWrapper/MySqlWatchRepositoryWrapper',
            'sourceWrapperClass' => 'Nette\Caching\Storages\FileStorage',
        ];

        $this->assertSame($expected, $config->getConfiguration());
    }

    public function testGetConfiguration2()
    {
        $config = new Configuration(__DIR__ . '/foo.neon');

        $this->expectException(\RuntimeException::class);
        $config->getConfiguration();
    }

    public function testGetConfiguration3()
    {
        self::$fileGetContentsOverridden = false;
        $config = new Configuration(__DIR__ . '/Assets/sampleConfig.neon');

        $this->expectException(\RuntimeException::class);
        $config->getConfiguration();
    }
}

namespace Solution;
use Tests\ConfigurationTest;
/**
 * Override file_get_contents() function in current namespace for testing.
 * @param string $path
 * @return bool
 */
function file_get_contents(string $path)
{
    return ConfigurationTest::$fileGetContentsOverridden ?? \file_get_contents($path);
}
