<?php
declare(strict_types=1);

namespace Tests\Solution;

use Assignment\XmlLoaderException;
use Assignment\XmlWatchLoader;
use PHPUnit\Framework\TestCase;
use Solution\Exception\InternalErrorException;
use Solution\Exception\WatchNotFoundException;
use Solution\SourceWrapper\XmlWatchLoaderWrapper;
use Solution\WatchController;

class XmlWatchLoaderWrapperTest extends TestCase
{
    public function testGetWatchData()
    {
        $loaderMock = $this->createMock(XmlWatchLoader::class);
        $loaderMock->expects($this->atLeastOnce())
            ->method('loadByIdFromXml')
            ->willThrowException(new XmlLoaderException());

        /** @var XmlWatchLoader $loader */
        $loader = $loaderMock;
        $wrapper = new XmlWatchLoaderWrapper($loader);

        $this->expectException(InternalErrorException::class);
        $wrapper->getWatchData(10);
    }

    public function testGetWatchData2()
    {
        $loaderMock = $this->createMock(XmlWatchLoader::class);
        $loaderMock->expects($this->atLeastOnce())
            ->method('loadByIdFromXml')
            ->willReturn(null);

        /** @var XmlWatchLoader $loader */
        $loader = $loaderMock;
        $wrapper = new XmlWatchLoaderWrapper($loader);

        $this->expectException(WatchNotFoundException::class);
        $wrapper->getWatchData(10);
    }

    public function testGetWatchData3()
    {
        $id = 10;
        $title = 'foo';
        $price = 20;
        $description = 'bar';

        $data = [
            'id' => $id,
            'title' => $title,
            'price' => $price,
            'desc' => $description,
        ];

        $loaderMock = $this->createMock(XmlWatchLoader::class);
        $loaderMock->expects($this->atLeastOnce())
            ->method('loadByIdFromXml')
            ->willReturn($data);

        /** @var XmlWatchLoader $loader */
        $loader = $loaderMock;
        $wrapper = new XmlWatchLoaderWrapper($loader);

        $data = $wrapper->getWatchData(10);
        $this->assertSame($id, $data[WatchController::RESULT_KEY_IDENTIFICATION]);
        $this->assertSame($title, $data[WatchController::RESULT_KEY_TITLE]);
        $this->assertSame($price, $data[WatchController::RESULT_KEY_PRICE]);
        $this->assertSame($description, $data[WatchController::RESULT_KEY_DESCRIPTION]);
    }
}
