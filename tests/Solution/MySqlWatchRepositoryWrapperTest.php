<?php
declare(strict_types=1);

namespace Tests\Solution;

use Assignment\MySqlRepositoryException;
use Assignment\MySqlWatchDTO;
use Assignment\MySqlWatchNotFoundException;
use Assignment\MySqlWatchRepository;
use PHPUnit\Framework\TestCase;
use Solution\Exception\InternalErrorException;
use Solution\Exception\WatchNotFoundException;
use Solution\SourceWrapper\MySqlWatchRepositoryWrapper;
use Solution\WatchController;

class MySqlWatchRepositoryWrapperTest extends TestCase
{
    public function testGetWatchData()
    {
        $repositoryMock = $this->createMock(MySqlWatchRepository::class);
        $repositoryMock->expects($this->atLeastOnce())
            ->method('getWatchById')
            ->willThrowException(new MySqlWatchNotFoundException());

        /** @var MySqlWatchRepository $repository */
        $repository = $repositoryMock;
        $wrapper = new MySqlWatchRepositoryWrapper($repository);

        $this->expectException(WatchNotFoundException::class);
        $wrapper->getWatchData(10);
    }

    public function testGetWatchData2()
    {
        $repositoryMock = $this->createMock(MySqlWatchRepository::class);
        $repositoryMock->expects($this->atLeastOnce())
            ->method('getWatchById')
            ->willThrowException(new MySqlRepositoryException());

        /** @var MySqlWatchRepository $repository */
        $repository = $repositoryMock;
        $wrapper = new MySqlWatchRepositoryWrapper($repository);
        $this->expectException(InternalErrorException::class);
        $wrapper->getWatchData(10);
    }

    public function testGetWatchData3()
    {
        $id = 10;
        $title = 'foo';
        $price = 20;
        $description = 'bar';

        $repositoryMock = $this->createMock(MySqlWatchRepository::class);
        $repositoryMock->expects($this->atLeastOnce())
            ->method('getWatchById')
            ->willReturn(new MySqlWatchDTO($id, $title, $price, $description));

        /** @var MySqlWatchRepository $repository */
        $repository = $repositoryMock;
        $wrapper = new MySqlWatchRepositoryWrapper($repository);

        $data = $wrapper->getWatchData(10);
        $this->assertSame($id, $data[WatchController::RESULT_KEY_IDENTIFICATION]);
        $this->assertSame($title, $data[WatchController::RESULT_KEY_TITLE]);
        $this->assertSame($price, $data[WatchController::RESULT_KEY_PRICE]);
        $this->assertSame($description, $data[WatchController::RESULT_KEY_DESCRIPTION]);
    }
}
