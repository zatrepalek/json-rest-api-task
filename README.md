Sample JSON REST API
====================

An underlying implementation of a controller acting as a gateway to a JSON REST API web service.

Requirements
------------

- PHP v7.1
- Composer
- (optional) Unix based system (for pre-commit hook)

Installation
------------

1. Clone repository:
    ```
    git clone git@gitlab.com:zatrepalek/json-rest-api-task.git
    ```
2. Install dependencies
    ```
    composer install
    ```
3. (optional) Set pre-commit hook (Unix based system only):
    ```
    cd json-rest-api-task;
    ln -s -f ../../pre-commit.sh .git/hooks/pre-commit;
    ```

Solution Notes
--------------

- Controller returns always JsonResponse. Request - response handling (e.g. routing to /watch/{id} and sending response)
is ommited (expecting this functionality as a part of project Framework). 

- All provided classes are put inside src/Assignment folder except implemented Solution/WatchController class in
Solution/WatchController. Classes are split into \Assignment and \Solution namespaces for better readability.

- Solution use NetteCache (storage can be configured as a service).

- Configuration is only implemented as skeleton. In real circumstances I would place this type of configuration inside
service definition part of config (i.e. this would be part of project Framework and is tightened to its dependency
injection architecture). Therefore cache or source could be configured as using appropriate service to be given inside
WatchController constructor. 

- Code is tested with unit tests only and src/ line coverage is 100%.
